#!/usr/bin/make -f
include /usr/share/ocaml/ocamlvars.mk

export LC_ALL=C.UTF-8

%:
	dh $@ --with ocaml

override_dh_auto_configure:
	if [ ! -d doc.backup ]; then cp -a doc doc.backup; fi
	touch config/Makefile.conf
	$(MAKE) clean
	rm config/Makefile.conf
	for ext in sub guess; do \
	  if [ -f /usr/share/misc/config.$$ext ] && \
	    ! [ -f debian/config.orig.$$ext ]; then \
	    mv config/config.$$ext debian/config.orig.$$ext; \
	    cp -f /usr/share/misc/config.$$ext config/config.$$ext; \
	  fi; \
	done
	./configure -galax-camllib $(OCAML_STDLIB_DIR)/galax			\
		-galax-camllib $(OCAML_STDLIB_DIR)/galax -galax-home /usr	\
		-galax-lib /usr/lib/galax -galax-man /usr/share/doc/galax/man	\
		-galax-usecases /usr/share/doc/galax/usecases			\
		-galax-regress /usr/share/doc/galax/regress			\
		-galax-examples /usr/share/doc/galax/examples			\
		-galax-config /usr/share/doc/galax/config -with-galaxd
	sed -i 's:^\(CONF_GALAX_\w\+\)=/:\1=$$(DESTDIR)/:' config/Makefile.conf

override_dh_auto_build:
	$(MAKE) world
	$(MAKE) -C doc/ manual.pdf manual.html

# remove .depend created during clean as they cause FTBFSs with -rsudo
override_dh_auto_clean:
	if [ -d doc -a ! -d doc.backup ]; then mv doc doc.backup; fi
	rm -Rf doc
	touch config/Makefile.conf
	$(MAKE) clean
	rm config/Makefile.conf
	if [ -d doc.backup ]; then mv doc.backup doc; fi
	for ext in sub guess; do \
	  if [ -f debian/config.orig.$$ext ] ; then \
	    mv -f debian/config.orig.$$ext config/config.$$ext; \
	  fi; \
	done

override_dh_auto_install:
	# $(LN) override so that we only install the best binary, instead of
	# both .opt and .byte together with the best symlink
	$(MAKE) install LN=cp DESTDIR=$(CURDIR)/debian/tmp
	rm -f debian/tmp/usr/bin/*.byte
	rm -f debian/tmp/usr/bin/*.opt
	# fix wrong permissions (+x) on some .mli files
	chmod 644 debian/tmp$(OCAML_STDLIB_DIR)/galax/*.mli
	# avoid cluttering usr/bin with too generic names
	mv debian/tmp/usr/bin/zerod  debian/tmp/usr/bin/galax-zerod
	mv debian/tmp/usr/bin/webgui debian/tmp/usr/bin/galax-webgui
	# files spotted by dh_missing that were never installed
	rm -f debian/tmp/usr/bin/ocaml-galax
	rm -f debian/tmp/usr/bin/galax-compile
	rm -f debian/tmp/usr/bin/galax-test
	# Lintian extra-license-file
	rm -f debian/tmp/usr/share/doc/galax/man/LICENSE

ifeq ($(OCAML_HAVE_OCAMLOPT),no)
override_dh_strip:
	dh_strip -Xusr/bin
endif

override_dh_install:
	dh_install -XCVS -Xlicense.tex

override_dh_compress:
	dh_compress -X.pdf

override_dh_dwz:
