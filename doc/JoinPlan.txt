

Join plan
---------

  0. Code clean-up

DONE  --> Pull the Btree operations out of the cs_code_sort_join. put
          that behind an interface.

      --> (try to use the shredded_dm_btree interface)

      --> Pull the calls to Hashtbl module from within
          cs_code_hash_join behind an interface (try with the
          shredded_dm_hash interface)

DONE  --> clean up the rid_list support

      --> clean up all those higher order functions which are hard to
          follow.

  1. Clean up predicates support in hash and sort joins

      --> fixing promotion bug
      --> make sure we handle simple type comparisons (op:integer-eq)
          properly
      --> make sure we handle cases without existential
          quantification.

 2. interfacing:

DONE  --> Add interface allowing to pass BTree indices as parameters
          to the sort-join.

      --> interface to the internal representation for predicates (see
          previous point?).

 3. staircase join:

      --> Add flag -index 'QNAME' in the processing model

ALTERNATIVE: Use a syntax in the query prolog!

      --> How do we pick up the index??

We need the code selection context to contain some of the following:
  --> list of indices built and a way to lookup the index based on the QName.

      --> Add streaming operation that takes an ordered typed stream,
          a list of QNames and builds on 2 BTrees -- one for preorder,
          one for postorder) for each QName. The stream operation
          returns the stream unchanged.

      --> Module supporting creation of the correct predicate
          (pre/post or pre/siblingpre) for use in the sortjoin.

      --> Add a new implementation of the tuple tree join using the
          new sort-join-index, picking up the btree indices created
          previously.

